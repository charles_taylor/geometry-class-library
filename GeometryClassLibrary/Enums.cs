﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeometryClassLibrary
{
    public class Enums
    {
        public enum Axis { X, Y, Z };
        public enum AxisPlanes { XYPlane, XZPlane, YZPlane };
    }
}
